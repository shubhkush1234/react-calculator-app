import React from 'react';

const UserInput = (props) => {
        return (
            <div>
                <label>Value 1</label><input type="text" name="value1" onChange={(event) => {
                    props.onInputChange(event)}} /><br /><br />
                <label>Value 2</label><input type="text" name="value2" onChange={(event) => {
                    props.onInputChange(event)}} />
            </div>
        )
    }
export default UserInput ;