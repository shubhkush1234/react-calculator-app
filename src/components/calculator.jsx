import React from 'react';
import Result from './Result';
import UserInput from './UserInput';

class Calculator extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            value1: '',
            value2: '',
            result: '',
            operator: ''
        };
    }

    onInputChange = (event) =>
        this.setState({
            [event.target.name]: event.target.value
        });

    Operations = (event) => {
        switch (event.target.value) {
            case 'add':
                this.setState({ result: parseInt(this.state.value1) + parseInt(this.state.value2), operator: '+' });
                break;
            case 'subtract':
                this.setState({ result: parseInt(this.state.value1) - parseInt(this.state.value2), operator: '-' });
                break;
            case 'multiply':
                this.setState({ result: parseInt(this.state.value1) * parseInt(this.state.value2), operator: '*' });
                break;
            case 'divide':
                this.setState({ result: parseInt(this.state.value1) / parseInt(this.state.value2), operator: '/' });
                break;
            default:
                this.setState({ result: "defaultResult" });
        }
    }

    render() {
        return (
            <div>
                <UserInput onInputChange={this.onInputChange} />
                <br /><br />
                <input type="button" onClick={this.Operations} value="add" />&nbsp;
                <input type="button" value="subtract" onClick={this.Operations} />&nbsp;
                <input type="button" value="multiply" onClick={this.Operations} />&nbsp;
                <input type="button" value="divide" onClick={this.Operations} />
                <div>Result {this.state.value1} {this.state.operator} {this.state.value2} = <Result clicked={this.state.result} /></div>
            </div>
        )
    }
}

export default Calculator;